#Camilo Lozano
import pygame, sys
from pygame.locals import *
pygame.init()
posA = 800
posB = 700
ventana = pygame.display.set_mode((posA,posB))
pygame.display.set_caption("Donkey Kong")
velocidad = 4
suelo = pygame.image.load ("suelo.png")
mario = pygame.image.load("mario.png")
mario_inv = pygame.transform.flip(mario,True,False);
clock = pygame.time.Clock()
plataforma1 = pygame.image.load("suelo_2.png")
princesa = pygame.image.load("peach.png")
barril = pygame.image.load("bar.png")
fuego = pygame.image.load("barril.png")
barril_movimiento = pygame.image.load("barril1.png")
br1 = pygame.image.load("barril1.png")
br2 = pygame.image.load("barril1.png")
br3 = pygame.image.load("barril1.png")
br4 = pygame.image.load("barril1.png")
br5 = pygame.image.load("barril1.png")
br6 = pygame.image.load("barril1.png")
br7 = pygame.image.load("barril1.png")
mono = pygame.image.load("donkey.png")
escalera_mala = pygame.image.load("escalerarota.png")
escalera_mala2 = pygame.image.load("escalerarota2.png")
escalera_mala3 = pygame.image.load("escalerarota3.png")
escalera_mala4 = pygame.image.load("escalerarota3.png")
base1 = pygame.image.load("suelo_3.png")
base2 = pygame.image.load("suelo_4.png")
base3 = pygame.image.load("suelo_4.png")
base4 = pygame.image.load("suelo_5.png")
base5 = pygame.image.load("suelo_6.png")
base6 = pygame.image.load("suelo_7.png")
base7 = pygame.image.load("suelo_8.png")
base8 = pygame.image.load("suelo_9.png")
base9 = pygame.image.load("Suelo_10.png")
base10 = pygame.image.load("suelo_9.png")
esc1 = pygame.image.load("escalera.png")
esc2 = pygame.image.load("escalera1.png")
esc3 = pygame.image.load("escalera3.png")
esc4 = pygame.image.load("escalera3.png")
esc5 = pygame.image.load("escalera5.png")
esc6 = pygame.image.load("escalera4.png")
c = pygame.sprite.Sprite()
k = pygame.sprite.Sprite()
k.mario_inv = mario_inv
k.rect = mario_inv.get_rect()
sueloa = pygame.Rect(0, 550, 960, 30)
c.image = mario
c.rect = mario.get_rect()
g = pygame.sprite.Sprite()
g.image = suelo 
g.rect = suelo.get_rect()
posX = 10
posY = 647
c.rect.top = posY
c.rect.left = posX
c.incremento_x = c.incremento_y = 0
c.velocidad = 1
c.valor_gravedad = 10
posf = 50
posg = 647
posm = 100
posn = 150
posb = 350
posf = 130
posj = 400
posk = 80
posa = 0
poss = 225
posl = 0
posc = 245
posl1=0
posc1=245
posl2=0
posc2=585
posl3=0
posc3=353
posl5=756
posc5=465
posl7=765
posc7=245

posX1 = posX
posY1 = posY
base = pygame.draw.rect(ventana,(0,0,0),(0,680,900,100))
extremol = pygame.draw.rect(ventana,(0,0,0),(0,0,1,800))
cielo = pygame.draw.rect(ventana,(0,0,0),(0,0,800,1))
extremor = pygame.draw.rect(ventana,(0,0,0),(800,0,1,800))
negro = (0,0,0)
velocidad_mono = 0.8
velocidad_barril = 2
velocidad_barril2 = 3
velocidad_barril3 = 2
velocidad_peach = 0.5
derecha = True
right = True
peach = True
aux = 1
z = True
y = True
l = True
p = True
h = True
Fuente = pygame.font.SysFont("Arial",30)
b1 = pygame.draw.rect(ventana,(200,0,0),(0,600,400,10))
b2 = pygame.draw.rect(ventana,(200,0,0),(230,600,200,10))
b3 = pygame.draw.rect(ventana,(200,0,0),(570,600,300,10))
b4 = pygame.draw.rect(ventana,(200,0,0),(400,600,120,10))
b5 = pygame.draw.rect(ventana,(200,0,0),(0,480,60,10))
b6 = pygame.draw.rect(ventana,(200,0,0),(100,480,300,10))
b7 = pygame.draw.rect(ventana,(200,0,0),(365,480,450,10))
b8 = pygame.draw.rect(ventana,(200,0,0),(0,370,300,10))
b9 = pygame.draw.rect(ventana,(200,0,0),(215,370,400,10))
b10 = pygame.draw.rect(ventana,(200,0,0),(660,370,200,10))
b11 = pygame.draw.rect(ventana,(200,0,0),(0,260,420,10))
b12 = pygame.draw.rect(ventana,(200,0,0),(415,260,300,10))
b13 = pygame.draw.rect(ventana,(200,0,0),(760,260,100,10))

aire = False
vm = 0

c.saltoY = 0
c.subiendo = False

def saltar():
    if c.saltoY == 0:
        c.saltoY = 80
        c.subiendo = True


        


#--------------BUCLE Principal del Programa-----------------------------------

while True:
    
    
    ventana.fill(negro) 
    ventana.blit(princesa,(posj,posk))
    ventana.blit(barril,(posa,poss))
    ventana.blit(suelo,(0,680))
    ventana.blit(plataforma1,(posb,posf))
    ventana.blit(fuego,(5,645))
    ventana.blit(mono,(posm,posn))
    ventana.blit(escalera_mala,(195,598))
    ventana.blit(escalera_mala2,(330,480))
    ventana.blit(escalera_mala3,(178,370))
    ventana.blit(escalera_mala4,(375,260))
    ventana.blit(base1,(0,600))
    ventana.blit(base2,(232,600))
    ventana.blit(base3,(570,600))
    ventana.blit(base4,(100,480))
    ventana.blit(base5,(0,480))
    ventana.blit(base6,(365,480))
    ventana.blit(base7,(0,370))
    ventana.blit(base8,(215,370))
    ventana.blit(base9,(0,260))
    ventana.blit(base10,(415,260))
    ventana.blit(esc1,(520,600))
    ventana.blit(esc2,(50,480))
    ventana.blit(esc3,(610,370))
    ventana.blit(esc4,(710,260))
    ventana.blit(esc5,(500,125))
    ventana.blit(esc6,(260,0))
    ventana.blit(c.image,c.rect)
    ventana.blit(barril_movimiento,(posl,posc))    
    ventana.blit(br2,(posl2,posc2))
    ventana.blit(br3,(posl3,posc3))    
    ventana.blit(br4,(posl5,posc5))    
    ventana.blit(br5,(posl7,posc7))

    
   
    
    time = pygame.time.get_ticks()/1000
    if aux == time:
        aux += 1
    contador = Fuente.render("Time: "+str(time),0,(120,70,0))
    ventana.blit(contador,(680,30))
    if (c.saltoY != 0):
        if c.subiendo == True:
            c.rect.top -= 4
        else:
            c.rect.top += 4

        c.saltoY -= 4

        if c.saltoY == 40:
            c.subiendo = False

    
            
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
            
    
        
        






#-------------ANIMACION MONO---------------------------------------

        
    if derecha == True:
        if posm<100:
            posm += velocidad_mono
        else:
            derecha = False
    else:
        if posm>30:
            posm -= velocidad_mono
        else:
            derecha = True

#-------------ANIMACION BARRIL----------------------------

    if c.rect.left >= posl and c.rect.left <= posl + 18 and c.rect.top + 18  >= posc and c.rect.top + 18 <= posc + 18:
        print("Game over")
        c.rect.left = posf
        c.rect.top = posg
    if right == True:
        if posl<756:
            posl += velocidad_barril
            
        else:
            right = False
    else:
        if posl>0:
            posl -= velocidad_barril
        else: 
            right = True
#____
   
#___

    if c.rect.left >= posl7 and c.rect.left <= posl7 + 18 and c.rect.top + 18  >= posc7 and c.rect.top + 18 <= posc7 + 18:
        print("Game over")
        c.rect.left = posf
        c.rect.top = posg
    if y == True:
        if posl7<765:
            posl7 += velocidad_barril2
            
        else:
            y = False
    else:
        if posl7>0:
            posl7 -= velocidad_barril2
        else: 
            y = True
#____

    if c.rect.left >= posl2 and c.rect.left <= posl2 + 18 and c.rect.top + 18  >= posc2 and c.rect.top + 18 <= posc2 + 18:
        print("Game over")
        c.rect.left = posf
        c.rect.top = posg
    if l == True:
        if posl2<800:
            posl2 += velocidad_barril
        
        else:
            l = False
    else:
        if posl2>0:
            posl2 -= velocidad_barril
        else: 
            l = True

#___
    if c.rect.left >= posl5 and c.rect.left <= posl5 + 18 and c.rect.top + 18  >= posc5 and c.rect.top + 18 <= posc5 + 18:
        print("Game over")
        c.rect.left = posf
        c.rect.top = posg
    if p == True:
        if posl5<765:
            posl5 += velocidad_barril3
            
        else:
            p = False
    else:
        if posl5>0:
            posl5 -= velocidad_barril3
        else: 
            p = True
#___#_____

    if c.rect.left >= posl3 and c.rect.left <= posl3 + 18 and c.rect.top + 18  >= posc3 and c.rect.top + 18 <= posc3 + 18:
        print("Game over")
        c.rect.left = posf
        c.rect.top = posg
    if h == True:
        if posl3<756:
            posl3 += velocidad_barril
            
        else:
            h = False
    else:
        if posl3>0:
            posl3 -= velocidad_barril
        else: 
            h = True
    
#--------------ANIMACION PRINCESA------------------------
    if peach == True:
        if posj<450:
            posj += velocidad_peach
        else:
            peach = False
    else:
        if posj>380:
            posj -= velocidad_peach
        else:
            peach = True

            
    
   
#-------------------TECLADO--------------------------
    moveLeft = False
    moveRight = False
             
    if event.type == pygame.KEYDOWN:
         if event.key == K_LEFT:
             moveLeft = True
           
         if event.key == K_RIGHT:
             moveRight = True

            
         if event.key == K_UP:
             if 544 > c.rect.left > 522:
                if 647 >= c.rect.top >= 569:
                   c.rect.top -= velocidad
             if 76> c.rect.left>52:
                if 567>=c.rect.top>447:
                   c.rect.top -= velocidad
             if 638 > c.rect.left >  610:
                if 447 >= c.rect.top > 335:
                    c.rect.top -= velocidad
             if 738 > c.rect.left > 710:
                if 355 >= c.rect.top > 227:
                    c.rect.top -= velocidad
             if 526 > c.rect.left > 502:
                 if 277 >= c.rect.top > 95:
                     c.rect.top -= velocidad                                           
         if event.key == K_DOWN:            
                 c.rect.top += velocidad

         if event.key == K_SPACE:
             saltar()
             ##if c.aire == False:
               ##     c.rect.top -= 80
                 ##   c.aire= True
                   

#-------------------------COLISIONES-------------------------------------
    

         if base.colliderect(c):
             c.rect.left = base.left
             c.rect.top = base.top
         if extremol.colliderect(c):
             c.rect.left = 0
             c.rect.top = extremol.top
         if cielo.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if extremor.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b1.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b2.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b3.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b4.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b5.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b6.colliderect(c):
            c.rect.left = posA
            c.rect.top = posB
         if b7.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b8.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b9.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b10.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b11.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b12.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
         if b13.colliderect(c):
             c.rect.left = posA
             c.rect.top = posB
             
         time = clock.tick(60)

         
                         
         pygame.display.flip()

         posA = c.rect.left
         posB = c.rect.top
         
    if event.type == pygame.KEYUP:
        if event.key == K_LEFT:
             moveLeft = False
           
        if event.key == K_RIGHT:
             moveRight = False
        
             
    if moveLeft:
        c.rect.left -= velocidad
    if moveRight:
        c.rect.left += velocidad
    
    pygame.display.update()                                                   
    

    
